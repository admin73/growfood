## Тестовое задание Growfood

Для того что бы развернуть проект, необходимо настроить подключение к бд в env.

Выполнить в корне проекта.
- **composer update**
- **npm install**
- **php artisan migrate**

## Для наполнения базы
- **tests/Feature/DataDbTest.php**

require('./bootstrap');

import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo)

//Main pages
import App from './views/app.vue'
import Order from './views/order'

const app = new Vue({
    el: '#app',
    components: {
        App,
        Order
    }
});

<?php

namespace App\Http\Controllers;

use App\Models\Tariff;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class TariffsController extends Controller
{
    /**
     * @var Tariff
     */
    protected Tariff $tariffModel;

    /**
     * @var array
     */
    protected array $results;

    /**
     * TariffsController constructor.
     */
    public function __construct()
    {
        $this->tariffModel = new Tariff();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getTariffsAndProducts(Request $request): JsonResponse
    {
        if ($tariffId = $request->get('tariff_id')) {
            $tariff = $this->tariffModel::find($tariffId);
            $this->results = [
                'id' => $tariff->id,
                'name' => $tariff->name,
                'price' => $tariff->price,
                'delivery' => $tariff->delivery,
                'products' => $tariff->products
            ];
        } else {
            $tariffs = $this->tariffModel::all();
            foreach ($tariffs as $tariff) {
                $this->results[] = [
                    'id' => $tariff->id,
                    'name' => $tariff->name,
                    'price' => $tariff->price,
                    'delivery' => $tariff->delivery,
                    'products' => $tariff->products
                ];
            }
        }

        return response()->json($this->results);
    }
}

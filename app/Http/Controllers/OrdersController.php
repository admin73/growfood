<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class OrdersController extends Controller
{
    protected Order $orderModel;

    protected User $user;

    public function __construct()
    {
        $this->orderModel = new Order();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createOrder(Request $request): JsonResponse
    {
        $this->user = (new UsersController)->getUserOrCreate($request);

        $order = $this->orderModel::firstOrCreate([
            'tariff_id' => (int)$request->get('tariff_id'),
            'user_id' => (int)$this->user->id,
            'delivery' => $request->get('delivery')
        ])->save();

        return response()->json($order);
    }
}

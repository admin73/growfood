<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Parents\UserData;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * @var User
     */
    protected User $userModel;

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->userModel = new User();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getUserOrCreate(Request $request)
    {
        $userData = UserData::fromRequest($request);

        if ($user = $this->userModel::where(['phone' => $userData->phone])->first()) {
            return $user;
        } else {
            return $this->userModel::firstOrCreate([
                'name' => $userData->name,
                'phone' => $userData->phone
            ]);
        }
    }
}

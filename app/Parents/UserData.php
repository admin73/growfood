<?php

namespace App\Parents;

use Illuminate\Http\Request;

final class UserData extends ObjectData
{
    public string $name;

    public string $phone;

    public static function fromRequest(Request $request): UserData
    {
        return new self([
            'name' => $request->get('name'),
            'phone' => $request->get('phone')
        ]);
    }
}
